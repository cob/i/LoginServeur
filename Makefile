SHELL=/bin/bash
BDD=./liste.csv
OUT=out/
#OPT=> /dev/null
listeclasse=$(shell sed -n 's/.*,\([123][a-zA-Z]\),.*/\1/p' ${BDD} | sort -u)
CIBLE=$(patsubst %, ${OUT}%.pdf, ${listeclasse})
TEX=$(patsubst %, ${OUT}%.tex, ${listeclasse})
PDF=pdflatex -output-directory ${OUT} $< ${OPT}
.SILENT:
.PRECIOUS: Makefile main.inc

.PHONY: all
all : ${CIBLE}

.PHONY: dossier
dossier:
	test -d ${OUT} || mkdir ${OUT}

${OUT}%.tex: dossier
	echo "Création de $@"
	cat main.inc > $@
	grep $(patsubst ${OUT}%.tex, %, $@) ${BDD} | sed 's/\([^,]*\),\([^,]*\),\([^,]*\),\([^,]*\),\([^,]*\),\([^,]*\)/\\MY{\1}{\2}{\3}{\4}{\5}/' >> $@
	echo "\end{document}" >> $@

${OUT}%.pdf : ${OUT}%.tex
	${PDF}

.PHONY: clean
clean :
	find ${OUT} -iname "*.aux" -delete
	find ${OUT} -iname "*.log" -delete

.PHONY: mrpropre
mrpropre : clean
	/bin/rm -rf ${OUT}

