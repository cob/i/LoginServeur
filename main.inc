\documentclass[a4paper,9pt]{scrartcl}
\usepackage[utf8]{inputenc}\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage[crossmark]{ticket}
% \usepackage[boxed]{ticket}

\unitlength=1mm
\hoffset=-16mm
\voffset=-21mm
\renewcommand{\ticketdefault}{}
\ticketNumbers{2}{7}
\ticketSize{96}{40}
\ticketDistance{0}{0}

\newcommand\myb[1]{\makebox[19mm][r]{\textit{\small #1} :\ }}
\newcommand{\MY}[5]{%
	\ticket{%
		\put( 5,33){%
			\begin{minipage}[t]{86mm}%
				\hfill\large #3 \bsc{#4}\hfill (#5)\\[.8ex]%
				\myb{courriel}#1@belluard.educanet2.ch\\%
				\myb{fichiers}ftp://ecole.co-belluard.ch\\%
				\myb{identifiant}#1%
				\myb{passe}#2\\%
				\myb{éducanet}https://www.educanet2.ch\\%
				\myb{moodle}http://moodle2.fri-tic.ch/my/\\%
			\end{minipage}%
		}%
	}%
}

\begin{document}
\sffamily

